package com.user.service;

import javax.validation.Valid;

import com.user.dto.AddEmployeeServiceDto;

public interface UserService {

	void addEmployee(@Valid AddEmployeeServiceDto employeeDetailsDto);

}
