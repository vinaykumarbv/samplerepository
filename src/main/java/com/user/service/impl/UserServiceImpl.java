package com.user.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.dto.AddEmployeeServiceDto;
import com.user.entity.EmployeeDetails;
import com.user.repository.UserRepository;
import com.user.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository employeeServiceRepository;

	

	@Override
	public void addEmployee(AddEmployeeServiceDto employeeDetailsDto) {
		String status = employeeDetailsDto.getEmployeeStatus();

		List<EmployeeDetails> employee = new ArrayList<EmployeeDetails>();

		// for(EmployeeDetailsDto employeeDto :employeeDetailsDto)
		employeeDetailsDto.getEmployeeDetailsDto().stream().forEach(employeeDto -> {
			EmployeeDetails employeeDetails = new EmployeeDetails();
			BeanUtils.copyProperties(employeeDto, employeeDetails);
			employeeDetails.setEmployeeStatus(status);
			employee.add(employeeDetails);

		});

		employeeServiceRepository.saveAll(employee);

		logger.info("Employee Added Successfully.");

	}

}
