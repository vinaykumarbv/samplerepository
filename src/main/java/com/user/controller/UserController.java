package com.user.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.user.dto.AddEmployeeServiceDto;
import com.user.service.UserService;



@RestController
public class UserController {
	@Autowired
	UserService employeeService;
	/**
	 * 
	 * @param employeeDetailsDto
	 * @return
	 */
	@PostMapping("/addEmployee")
	public ResponseEntity<String>  addEmployee(@RequestBody @Valid AddEmployeeServiceDto employeeDetailsDto ) {
		employeeService.addEmployee(employeeDetailsDto);
		return new ResponseEntity<String>("Employee Added Successfully",HttpStatus.ACCEPTED);
		
	}
	

}
